// Copyright (c) 2019 Abanoub Sameh

#include <stdio.h>

// this is just a test program
int main() {
        printf("guess a number from 0 to 100\n");

        int x;
        scanf("%d", &x);

        while (x != 88) {       // you are stuck in the loop until you guess the right number
                printf("you are wrong\n");
                scanf("%d", &x);
        }

        printf("you are right\n");
}
