// Copyright (c) 2019 Abanoub Sameh

#include <stdio.h>      // need to include this to use some functions like:
                        // printf, scanf and a lot more

int main() {            // main is a function like any other fuction
                        // but it is the first function that runs
// ^ here main has a type int because it is running in an OS so it returns a result to it

        printf("Hello World!\n"); // function from the the stdio.h
}
