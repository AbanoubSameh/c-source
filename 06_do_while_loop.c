// Copyright (c) 2019 Abanoub Sameh

#include <stdio.h>

int main() {
        printf("guess a number from 0 to 100\n");
        int x;

        do {    // do while loop is the same as while
                // but it RUNS FIRST and checks the condition later
                scanf("%d", &x);
                printf("you are wrong\n");

        } while (x != 10);

        printf("you are right\n");
}
