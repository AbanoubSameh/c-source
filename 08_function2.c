// Copyright (c) 2019 Abanoub Sameh

#include <stdio.h>

// this function takes two arguments and returns an int
int al_quadrato(int x, int y) {
        return y * x;
}

int main() {
        int h;

        printf("enter a number\n");
        scanf("%d", &h);

        int x = al_quadrato(h, h);      // here we call the function
        printf("%d\n", x);
}
