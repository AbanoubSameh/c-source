// Copyright (c) 2023 Abanoub Sameh

#include <stdio.h>

// this is a definition of a struct
// this tells the compiler how to create a point struct when we want to
// this definition does not use memory
struct point {
        // a struct stores many variables of different types
        float x;
        float y;
        char c;
};

typedef struct {
// ^ we can use the new struct to creat a new type, which will be added to the compiler types
// such as int, char, float ...

        struct point points[2];         // a struct can contain an array
} line;

typedef // we define that this struct

struct {
        struct point center;            // a struct can contain other stucts
        float radius;
}

circle; // is to be created when ever we use the type circle

// we can define ane type that we want:
typedef int my_new_int_type;            // this can be used just like int, char, float ...

int main()
{
        // create a new variables of type struct point
        struct point m = { 10, 6, 'm' };        // this is storing two floats and then a character [float][float][char]
                                                //                                                   10     6      'm'
        struct point n = { 20, 16, 'n' };
        struct point s = { 21, 28, 's' };
        struct point t = { 6, 96, 't' };

        // we don't need to use struct, since the compiler now has new type called line
        line line1 = { m, n };

        // a line struct contains a point array of size 2:      [point][point]
        // which is equal to:                                   [float][float][char][float][float][char]

        line line2 = { s, t };

        circle circle1 = {
                { 26, 40, 'c' },        // we can define a point struct directly in here
                10
        };

        // a circle struct contains a point and a float:        [point][int]
        // which is equal to:                                   [float][float][char][int]

        printf("point %c: is at (%0.2f, %0.2f)\n", m.c, m.x, m.y);
                                        // we use the    ^ period operator to get the values inside the struct

        // m.c points to        [float][float][char]
        //                                      ^

        // m.x points to        [float][float][char]
        //                         ^

        // m.y points to        [float][float][char]
        //                                ^

        printf("point %c: is at (%0.2f, %0.2f)\n", n.c, n.x, n.y);
                                        // every struct has its own c, x, and y

        printf("line %c_%c: (%0.2f, %0.2f)\n", line1.points[0].c, line1.points[1].c);
                                        // we use ^ to get the points array  ^ to get the nth element of the array and print c

        // line1.points[0].c points to  [float][float][char][float][float][char]
        //                                              ^

        // line1.points[1].c points to  [float][float][char][float][float][char]
        //                                                                  ^

        // line1.points[0].x points to  [float][float][char][float][float][char]
        //                                 ^

        // line1.points[0].y points to  [float][float][char][float][float][char]
        //                                        ^

        // line1.points[1].x points to  [float][float][char][float][float][char]
        //                                                     ^

        // line1.points[1].y points to  [float][float][char][float][float][char]
        //                                                            ^

        printf("the circle radius is %0.2f\n", circle1.radius);

        circle1.radius = 20;
        // we can also change the value inside a struct using the period operator

        // a circle is          [point][float]
        // which is equal to    [float][float][char][float]
        // so this puts 20 into                        ^

        printf("the circle radius is %0.2f\n", circle1.radius);

        // we can have an array of structs:
        circle my_circles[5];
        // which is equal to    [float][float][char][float][float][float][char][float][float][float][char][float][float][float][char][float][float][float][char][float]

        // you can use my type just like a normal int
        my_new_int_type x = 35;
        printf("%d\n", x);
}
