// Copyright (c) 2019 Abanoub Sameh

#include <stdio.h>

int main() {
    int x = 8;

    if (x == 7) {               // since x is not equal to 7 this will not run
        printf("x is equal seven\n");
    } else if (x == 9) {        // same thing here
        printf("x is equal nine\n");
    } else {                    // this will run only if all the other conditions are false
        printf("x is equal eight\n");
    }
}
