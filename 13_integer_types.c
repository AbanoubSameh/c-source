// Copyright (c) 2023 Abanoub Sameh

#include <stdint.h>
#include <stdio.h>

int main()
{
        // an integar can be unsigned or signed
        // e.g. the unsigned            01001110
        //                             |       | represent the number
        // while the signed          0   1001110
        //                           ^  |      | this parts represent the number in 2's complement
        //                           |
        //                         and first bit is the sign

        // a variable with a larger size can represent larger numbers

        // unsigned types, cannot hold negative numbers
        uint8_t x1 = 215;                       // 1 byte, from 0 to 255
        uint16_t x2 = 23623;                    // 2 bytes, from 0 to 65535
        uint32_t x3 = 2362362323;               // 4 bytes, from 0 to 4294967295
        uint64_t x4 = 3202592350235235;         // 8 bytes, from 0 to 18446744073709551615

        printf("maximum uint8_t size: %u\nmaximum uint16_t size: %u\n", UINT8_MAX, UINT16_MAX);
        printf("maximum uint32_t size: %u\nmaximum uint64_t size: %lu\n", UINT32_MAX, UINT64_MAX);
                        // we use u     ^ to print unsigned types, ^ lu to print 64 unsigned integers

        // we would get the wrong result if we treat a negative number as unsigned
        // e.g. -2 would print out the wrong data
        printf("wrong number: %u\n", -2);

        // we would get the wrong result if we try to store a number greater than the max size
        // we might also get a warning and/or an error depending on the compiler
        uint8_t too_small = 322362323;
        printf("wrong number: %u\n", too_small);

        // signed types, can hold negative numbers, but are smaller
        // since we need one bit to signify the sign +/-
        int8_t y1 = -125;                       // 1 byte, from -128 to +127
        int16_t y2 = 23623;                     // 2 bytes, from -32768 to 32767
        int32_t y3 = 362362325;                 // 4 bytes, from -2147483648 to +2147483647
        int64_t y4 = -235623623674373;          // 8 bytes, from -9223372036854775808 to 9223372036854775807

        printf("maximum int8_t size: %d\nmaximum int16_t size: %d\n", INT8_MAX, INT16_MAX);
        printf("maximum int32_t size: %d\nmaximum int64_t size: %ld\n", INT32_MAX, INT64_MAX);
                                                        // we use ^ ld to print 64 bit integers

        printf("minimum int8_t size: %d\nminimum int16_t size: %d\n", INT8_MIN, INT16_MIN);
        printf("minimum int32_t size: %d\nminimum int64_t size: %ld\n", INT32_MIN, INT64_MIN);

        // we would get the wrong result sometimes if we print an un signed number
        // as an unsigned
        printf("wrong number: %d\n", (uint32_t) 2236236236);
        printf("not a wrong number: %d\n", (uint32_t) 32236);

        // again we would get the wrong result if we try to store a number greater than the max size
        // we might also warnings and/or an error depending on the compiler
        int8_t signed_too_small1 = 2376323;
        int8_t signed_too_small2 = -322362323;
        printf("wrong numbers: %u, %u\n", signed_too_small1, signed_too_small2);

        // there are bigger types but they are not always available and are hard to use
        __uint128_t very_big_number = 340282366920938463463374607431768211456;
        __int128_t very_big_number_negative_number = -282366920938463463374607431768211456;
}
