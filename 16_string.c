// Copyright (c) 2023 Abanoub Sameh

#include <stdio.h>

int main()
{
        char string[] = "Hello World";  // we use "" to store a string

        // a string is just a character array that ends with the NULL character
        // which has the code 0
        // the compiler add the NULL charcter automatically so
        // "Hello World" is equal to "Hello World\0"
        // equal to [H][e][l][l][o][ ][W][o][r][l][d][\0]

        printf("%s\n", string);
        //       ^ we use s to print a string

        // we can assign a string one character at a time
        char my_name[10];       // this stores 10 chars: [char][char][char][char][char][char][char][char][char][char]

        my_name[0] = 'T';
        my_name[1] = 'e';
        my_name[2] = 's';
        my_name[3] = 'l';
        my_name[4] = 'a';
        my_name[5] = '\0';      // a string needs to end with the NULL character
                                // since my_name is an empty char array
                                // we need to add the null character
                                // to terminate the string manually

        // now my_name is [T][e][s][l][a][\0][char][char][char][char]
        //                                     ^     ^     ^     ^
        //                                  those characters may contain 0 or any random data

        printf("%s\n", my_name);

        // the NULL character tells the compiler that this is the end of the string
        // and it stops printing when it sees the NULL character

        // if we add the NULL character to the middle of a string
        // the rest of the string will not be printed

        my_name[2] = '\0';

        // now my_name is [T][e][\0][l][a][\0][char][char][char][char]

        printf("%s\n", my_name);

        // if a string does not have a NULL character at the end
        // or if we overwrite it
        // we can cause issues if we try to copy the string, manipulate it or print it
        // since we may copy random data, print random data
        // or read the wrong memory address and the OS will terminate the program

        my_name[0] = 'c';
        my_name[1] = 'r';
        my_name[2] = 'a';
        my_name[3] = 's';
        my_name[4] = 'h';
        my_name[5] = ' ';
        my_name[6] = 'm';
        my_name[7] = 'e';

        // now my_name is [c][r][a][s][h][ ][m][e][char][char]

        // it has not NULL character, if you uncomment the next the line you might print random data or crash the program:
        //printf("%s\n", my_name);
}
