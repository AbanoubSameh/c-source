// Copyright (c) 2023 Abanoub Sameh

#include <stdio.h>

// this is a definition of an enum (an enumerated type)
// which is a type that can have only certain values
// this tells the compiler who many enum variants there are
// this definition does not use memory
enum weekday {
        monday,
        tuesday,
        wednesday,
        thursday,
        friday,
        saturday,
        sunday,
};

// we can use typedef with enums too
typedef enum {
        january,
        february,
        march,
        aprile = 4,         // we can define numbers for different variants
        may = 5,
        june,
        july,
        august,
        september,
        october,
        november,
        december,
} month;

int main()
{
        // we can assign an enum with any of its types
        enum weekday day = sunday;

        // we can also treat an enum just like an int
        day = 2;

        // with a typedef we don't need to write enum in front of the variable
        month this_month = november;

        // we can compare an enum to one of its variants
        if (this_month == october) {
                printf("it is still summer\n");
        } else if (this_month == december) {
                printf("it is getting colder\n");
        } else if (this_month == january) {
                printf("it is really cold!!\n");
        } else if (this_month == 4) {
                        //       ^ we can also compare to a number (in this case 4 is aprile)
                printf("spring is starting");
        }

        // we can print an enum, and it will print its value
        // it will print a int, and not its name
        printf("it is %d, you need to go to work\n", monday);
}
