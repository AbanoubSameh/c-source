// Copyright (c) 2023 Abanoub Sameh

#include <stdio.h>

int main()
{
        char p = 'p';                           // we use '' to store a character
        printf("a character: %c\n", p);

        // a character is just a 1 byte number (uint8_t)
        // if we print the character as a number, it will print the character code
        printf("same character: %d\n", p);

        char h = 104;                           // if can store a number by using its character code
        // 104    ^ is the code for 'h'
        // this is equal to
        // char h = 'h';

        // it will print normally
        printf("another character: %c\n", h);

        // we can print a number as a character if we use its code
        int x = 65;     // 65 is the code of the letter 'A'

        printf("yet another character: %c\n", x);

        // numbers have characters too
        // e.g. 6 is different than '6'

        char char_six = '6';    // this is the character 6, which is the number 54
        int digit_six = 6;      // this is the number 6

        printf("'6' as a number: %d\n6 as a number: %d\n", char_six, digit_six);
        printf("'6' as a character: %c\n", char_six);

        // 6 as a character cannot be printed since it is a control character
        printf("6 as a character: %c\n", digit_six);

        // we can print the character '6' by using its code, which is 54
        printf("'6' as a character: %c\n", 54);

        // other symbols are just character too
        // such as / \ ( ) { } [ ] , . % ^ ...
        // even space is the character ' ' with code 32
        for (int i = 32; i < 127; i++) {
                printf("code %d, is char '%c'\n", i, i);
        }

        // some characters are control characters, unprintable character, white space
        // such as new line: '\n', tab: '\t', NULL character: '\0', ...
        // those character are from code 0 to 31, and 32 is space ' '

        char new_line = '\n';
        printf("first line%csecond line\n", new_line);
}
