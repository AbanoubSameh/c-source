// Copyright (c) 2023 Abanoub Sameh

#include <stdio.h>

int main()
{
        // this is a variable of type int
        int sum;        // this stores a single int: [int]

        // this is an array (a number of elements of the same type)
        int x[10];      // this stores 10 ints: [int][int][int][int][int][int][int][int][int][int]

       scanf("%d", x);
                // ^ x points to the first address in the array: [int][int][int][int][int][int][int][int][int][int]
                //                                                 ^
                // we don't need to use & since x is already a pointer not a value

       scanf("%d", x + 1);
                // ^ x + 1 points to the second address in the array: [int][int][int][int][int][int][int][int][int][int]
                //                                                           ^

       scanf("%d", x + 2);
                // ^ x + 2 points to the second address in the array: [int][int][int][int][int][int][int][int][int][int]
                //                                                                ^

       scanf("%d", x + 3);
       scanf("%d", x + 4);
       scanf("%d", x + 5);
       scanf("%d", x + 6);
       scanf("%d", x + 7);
       scanf("%d", x + 8);
       scanf("%d", x + 9);
       // there is no x + 10, since we start at 0 we count up to 9

        sum += x[0];    // this is equivalent to sum = sum + x[0]
        //       ^ we use this to get the first element in the x array: [int][int][int][int][int][int][int][int][int][int]
        //                                                                ^

        sum += x[1];
        //       ^ we use this to get the second element in the x array: [int][int][int][int][int][int][int][int][int][int]
        //                                                                      ^

        sum += x[2];
        //       ^ we use this to get the second element in the x array: [int][int][int][int][int][int][int][int][int][int]
        //                                                                           ^

        sum += x[3];
        sum += x[4];
        sum += x[5];
        sum += x[6];
        sum += x[7];
        sum += x[8];
        sum += x[9];
        // again we go from 0 to 9, which is 10 elements

        printf("the sum is: %d\n", sum);

        // we can have a multidentional array (an array of arrays)
        int matrix[2][3];
        // whichi is equal to   [int][int][int]
        //                      [int][int][int]

        // we can have more dimentions
        int matricies[2][3][2];

        // whichi is equal to   [int][int][int]
        //                      [int][int][int]
        //
        //                      [int][int][int]
        //                      [int][int][int]

        // we can have even more dimentions
        int more_matricies[2][3][2][4][6][2];
}
