// Copyright (c) 2019 Abanoub Sameh

#include <stdio.h>

int main() {
        int x;  // need a place to store a value

        printf("please enter a number\n");
        scanf("%d", &x);        // %d means that we are looking for an int
                //  ^ the & means the address of x
        printf("you entered: %d\n", x);
                                //  ^ no & because we need x itself
}
