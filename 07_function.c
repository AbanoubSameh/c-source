// Copyright (c) 2019 Abanoub Sameh

#include <stdio.h>

/* every function has a type and inputs (prameters), this function has
type int (it returns an int as a result)
and has one input (you need to give it this input each time you call it) */
int raddopiare(int x) {
        return 2 * x;
}

// a function that does not return a value is declared void, like this:
void do_nothing() {
            // ^ this function takes no arguments
}

int main() {
        int x = raddopiare(3);
        printf("%d\n", x);
}
