// Copyright (c) 2019 Abanoub Sameh

#include <stdio.h>

int main() {
        int counter = 0;
        while (counter < 1000) {        // a while loop needs a variable to count
                                        // or a condition that changes
                                        // else it will run for ever
                printf("Hello\n");

                counter++;      // equivalent to counter = counter + 1
                //  ^ always remembe this line otherwise it will run forever
        }
}
