// Copyright (c) 2023 Abanoub Sameh

#include <float.h>
#include <stdio.h>

int main()
{
        // an float is just a number multiplied by an exponent
        // just like:                   125622 * 10^23
        // but in binary we use base 2: 011011 * 2^21
        // and can have different sizes
        // a float with a larger size can represent larger numbers
        // there are two sizes float and double

        // a 32 bit float has a sign bit, an 8 bit exponent
        // and a 23 bit number:   0    01001101    01011110001010101110011
        //                        ^   |       |   |                      | the number
        //                        |   |       |
        //                        |   |       | the exponent
        //                        |
        //                        | the sign bit

        // a 64 bit float has a sign bit, an 11 bit exponent
        // and a 52 bit number:   0    01001101001    0101111000101010111001111000010010110101111100000011
        //                        ^   |          |   |                                                   | the number
        //                        |   |          |
        //                        |   |          | the exponent
        //                        |
        //                        | the sign bit

        // a 32 bit float has about 7 digits of precision
        float pi = 3.141592;                                    // this is printed as is
        float more_pi = 3.14159265358979323;                    // 3.141592 is approximated to  3.141593
                                                                // since the next digit is 6:   3.1415926

        // a 64 bit float has about 16 digits of precision
        double even_more_pi = 3.141592653589793;                // this is printed as is
        double very_long_pi = 3.141592653589793238462643383279; // this is approximated to      3.141592653589793
                                                                // since the next digit is 2:   3.141592653589793

        // if you try to store more you will lose precision, get an approximation, or get the wrong results
        printf("32 bit pi:\t\t%f\nmore of 32 bit pi:\t%f\n", pi, more_pi);
        printf("64 bit pi:\t\t%.20f\nmore of 64 bit pi:\t%.20f\n", even_more_pi, very_long_pi);
        printf("real pi:\t\t3.141592653589793238462643383279502884197\n");

        float test1 = 16.00001;
        float test2 = 16.000001;
        float test3 = 16.0000001;

        float test4 = 16.00005;
        float test5 = 16.000005;
        float test6 = 16.0000005;

        printf("precise number:\t\t%f\nless precise:\t\t%f\nlost precision:\t\t%f\n", test1, test2, test3);
        printf("precise number:\t\t%f\nless precise:\t\t%f\nlost precision:\t\t%f\n", test4, test5, test6);
}
