// Copyright (c) 2019 Abanoub Sameh

#include <stdio.h>

int main() {
        /*
        printf("Hello\n");
        printf("Hello\n");
        printf("Hello\n");
        printf("Hello\n");
        printf("Hello\n");

        printf("Hello\n");
        printf("Hello\n");
        printf("Hello\n");
        printf("Hello\n");
        printf("Hello\n");
        */

        for (int i = 0; i < 10; i++) {  // instead of repeating code we just use a loop
                printf("Hello %d\n", i);        // i has a new value each time
        }

}
